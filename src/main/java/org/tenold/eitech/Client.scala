package org.tenold.eitech

import java.io.{BufferedReader, IOException, InputStreamReader, PrintWriter}
import java.net.Socket

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill}

class Client(socket: Socket, roomActorRef: ActorRef, userRef: ActorRef) extends Actor with Runnable with ActorLogging {

  private val printWriter: PrintWriter = new PrintWriter(socket.getOutputStream, true)
  private val bufferedReader: BufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream))

  private val prettyMode: Boolean = true
  private var isAlive: Boolean = true
  private var inRoom: String = _

  new Thread(this).start()

  send("< Hello!")
  send("< What is your name?")

  override def receive = {
    case IncomingMessage(message) if message == "/quit" => quit()
    case IncomingMessage(message) if message.startsWith("/join ") => userRef ! JoinRoomAction(message.replaceFirst("/join ", ""), null)
    case IncomingMessage(message) if message == "/leave" => userRef ! LeaveRoomAction
    case IncomingMessage(message) if message == "/rooms" => userRef ! ListRoomsAction
    case IncomingMessage(message) if message == "/help" => help()
    case genericMessage: IncomingMessage => userRef ! genericMessage

    case event: JoinedRoomNotification => inRoom = event.name

    case notification: Notification => handleNotification(notification)

    case msg => unhandled(msg)
  }

  private def help() {
    send("The following commands are available in the lobby:")
    send("/rooms              List available rooms")
    send("/join <roomname>    Join or create a room named <roomname>")
    send("/help               Show help")
    send("/quit               Exit the chat")
    send("")
    send("The following commands are available in a room:")
    send("/leave              Show help")
    send("/quit               Exit the chat")
  }

  private def quit() {
    userRef ! QuitAction
    isAlive = false
    printWriter.close()
    bufferedReader.close()
    socket.close()
    self.tell(PoisonPill, self)
  }

  private def handleNotification(notification: Notification) = {
    notification match {
      case notification: ServerNotification =>
        if (prettyMode) printWriter.write("\u001B[1m< " + notification.message + "\u001B[0m\n")
        else printWriter.write("< " + notification.message + "\n")
        //            printWriter.write(" \u2713 Check mark showed \n");
        printWriter.flush()
      case notification: RoomNotification =>
        if (prettyMode) printWriter.write("\u001B[1m#" + notification.room + "\t" + notification.message + "\u001B[0m\n")
        else printWriter.write("#" + notification.room + "\t" + notification.message + "\n")
        //            printWriter.write(" \u2713 Check mark showed \n");
        printWriter.flush()

      case notification: RoomMessageNotification => send("#" + notification.room + "\t" + notification.sender + ":\t" + notification.message)

      case msg =>
        log.warning("Unhandled message: {}", msg)
        unhandled(msg)
    }
  }

  private def send(msg: String): Unit = {
    printWriter.write(msg + "\n")
    printWriter.flush()
  }

  override def run() = {
    try
        while (isAlive) {
          val s = bufferedReader.readLine
          self.tell(IncomingMessage(s), self)
        }
    catch {
      case e: IOException =>
        if (e.getMessage != "Socket closed") e.printStackTrace()
    }
  }
}
