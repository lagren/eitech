package org.tenold.eitech

sealed trait Action

sealed trait RoomAction {
  def roomName: String
}

case class JoinRoomAction(roomName: String, nick: String) extends RoomAction
case class LeaveRoomAction(roomName: String, nick: String) extends RoomAction
case object ListRoomsAction extends Action

case class SayInRoomAction(nick: String, message: String, roomName: String) extends RoomAction

case class IncomingMessage(message: String)

case object QuitAction