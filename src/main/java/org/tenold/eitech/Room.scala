package org.tenold.eitech

import akka.actor.{Actor, ActorLogging, ActorRef}

import scala.collection.mutable

class Room extends Actor with ActorLogging {

  private val participants = mutable.HashMap[String, ActorRef]()
  private var name: String = _

  override def receive = {
    case msg: InitRoom => initRoom(msg)
    case msg: JoinRoomAction => joinRoom(msg)
    case msg: SayInRoomAction => sayInRoom(msg)
    case msg: LeaveRoomAction => leaveRoom(msg)
    case msg =>
      log.warning("Room: Unknown message: {}", msg)
      unhandled(msg)
  }

  private def leaveRoom(msg: LeaveRoomAction) = {
    log.debug("User {} is leaving room {}", msg.nick, name)
    participants.remove(msg.nick)

    participants.values.foreach(a => a ! RoomNotification(name, msg.nick + " has left the room"))
  }

  private def sayInRoom(msg: SayInRoomAction) = {
    participants
      .values
      .filter(a => !a.equals(sender()))
      .foreach(a => a ! RoomMessageNotification(name, msg.message, msg.nick))
  }

  private def initRoom(msg: InitRoom) = {
    log.info("Creating new room: {}", msg.name)

    name = msg.name
  }

  private def joinRoom(msg: JoinRoomAction) = {
    participants.put(msg.nick, sender)

    sender ! JoinedRoomNotification(name)
    sender ! RoomNotification(name, "Welcome to " + name + ", " + msg.nick + "!")

    if (participants.size == 1) sender ! RoomNotification(name, "You are the only participant. Maybe you want to invite some friends?")
    else {
      sender.tell(RoomNotification(name, "Participants in this room:"), self)
      import scala.collection.JavaConversions._
      for (stringActorRefEntry <- participants.entrySet) {
        if (stringActorRefEntry.getValue ne sender) sender.tell(RoomNotification(name, "* " + stringActorRefEntry.getKey), self)
      }
    }
    // Tell others
    participants.values.filter(actor => !actor.equals(sender())).foreach(a => a ! RoomNotification(name, msg.nick + " joined the room"))
  }

}

case class InitRoom(name: String)