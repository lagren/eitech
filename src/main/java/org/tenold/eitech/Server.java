package org.tenold.eitech;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {
        int port = 1234;

        ServerSocket serverSocket = new ServerSocket(port);

        ActorSystem actorSystem = ActorSystem.create("eitech");

        final ActorRef roomDispatcherActorRef = actorSystem.actorOf(Props.create(RoomDispatcherActor.class), "rooms");
        final ActorRef userRegistry = actorSystem.actorOf(Props.create(UserRegistry.class));

        System.out.println("Listening on port " + port + " for incoming connections...");

        while(true) {
            final Socket accept = serverSocket.accept();
            System.out.println("New client connection from " + accept.getRemoteSocketAddress());

            final ActorRef userRef = actorSystem.actorOf(Props.create(User.class, roomDispatcherActorRef, accept, userRegistry));
        }
    }
}
