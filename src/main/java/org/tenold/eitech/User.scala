package org.tenold.eitech

import java.net.Socket

import akka.actor.{ActorRef, FSM, PoisonPill, Props}

class User(rooms: ActorRef, socket: Socket, userRegistry: ActorRef) extends FSM[State, Data] {

  val client: ActorRef = context.system.actorOf(Props.create(classOf[Client], socket, rooms, self))

  startWith(Registering, Uninitialized)

  when(Registering) {
    case Event(IncomingMessage(nickname), Uninitialized) =>
      userRegistry ! Register(nickname)
      stay

    case Event(RegistrationSuccessful(nickname), _) =>
      client ! ServerNotification("Welcome, " + nickname + "!")
      goto(InLobby) using UserData(nickname)

    case Event(RegistrationFailed(nickname), _) =>
      client ! ServerNotification(s"Sorry, please choose another username. $nickname is already taken.")
      stay

  }

  when(InLobby) {
    case Event(IncomingMessage(_), UserData(_, _)) =>
      stay

    case Event(JoinRoomAction(roomName, _), UserData(username, _)) =>
      goto(InRoom) using UserData(username, Some(roomName))

    case Event(ListRoomsAction, _) =>
      rooms ! ListRoomsAction
      stay
  }

  when(InRoom) {
    case Event(IncomingMessage(message), UserData(nick, Some(room))) =>
      rooms ! SayInRoomAction(nick, message, room)
      stay

    case Event(msg: SayInRoomAction, _) =>
      rooms ! msg
      stay

    case Event(LeaveRoomAction, UserData(nick, Some(_))) =>
      goto(InLobby) using UserData(nick, None)
  }

  onTransition {
    case InLobby -> InRoom =>
      nextStateData match {
        case UserData(username, Some(room)) => rooms ! JoinRoomAction(room, username)
        case _ => stay
      }

    case InRoom -> InLobby =>
      stateData match {
        case UserData(nickname, Some(roomName)) =>
          rooms ! LeaveRoomAction(roomName, nickname)
          client ! RoomNotification(roomName, "You have left the room")
          client ! ServerNotification("You are now in the lobby")
        case _ => stay
      }
  }

  whenUnhandled {
    case Event(n: Notification, _) =>
      client ! n
      stay
    case Event(ListRoomsAction, _) => stay
    case Event(QuitAction, UserData(nickname, _)) =>
      userRegistry ! Unregister(nickname)
      self ! PoisonPill
      stay
  }

}



