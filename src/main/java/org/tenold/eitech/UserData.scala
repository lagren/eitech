package org.tenold.eitech

sealed trait Data

case object Uninitialized extends Data

case class UserData(nickname: String, roomName: Option[String] = None) extends Data