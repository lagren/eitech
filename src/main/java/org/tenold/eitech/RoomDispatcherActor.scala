package org.tenold.eitech

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

import scala.collection.mutable

class RoomDispatcherActor extends Actor with ActorLogging {

  val rooms: mutable.Map[String, ActorRef] = mutable.HashMap()

  def receive = {
    case msg: JoinRoomAction => joinRoom(msg)
    case msg: SayInRoomAction => sayInRoom(msg)
    case msg: LeaveRoomAction => leaveRoom(msg)
    case ListRoomsAction => listRooms()
    case msg =>
      log.warning("Unhandled message: {}", msg)
      unhandled(msg)
  }

  def listRooms() {
    sender() ! ServerNotification("List of rooms:")

    rooms.keys.foreach{ roomName =>
      sender() ! ServerNotification("* "  + roomName)
    }
    sender() ! ServerNotification("End of list")
  }

  def leaveRoom(msg: LeaveRoomAction) {
    val roomLookupKey = normalized(msg.roomName)

    rooms.get(roomLookupKey).foreach(_ forward msg)
  }

  def joinRoom(msg: JoinRoomAction) {
    val roomLookupKey = normalized(msg.roomName)

    val roomActorRef = rooms.get(roomLookupKey)

    if (roomActorRef.isDefined) {
      roomActorRef.get forward msg
    } else {
      val roomActorRef = context.actorOf(Props(classOf[Room]), roomLookupKey)
      roomActorRef.tell(InitRoom(msg.roomName), self)
      roomActorRef forward msg
      rooms.put(roomLookupKey, roomActorRef)
    }
  }

  def sayInRoom(msg: SayInRoomAction) {
    val roomLookupKey = normalized(msg.roomName)

    rooms.get(roomLookupKey).foreach(_ forward msg)
  }

  private def normalized(name: String): String = {
    name.toLowerCase.trim
  }

}
