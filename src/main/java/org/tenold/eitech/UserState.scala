package org.tenold.eitech

sealed trait State

case object Registering extends State
case object InLobby extends State
case object InRoom extends State
case object LoggedOut extends State