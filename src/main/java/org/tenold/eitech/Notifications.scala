package org.tenold.eitech

sealed trait Notification

case class ServerNotification(message: String) extends Notification
case class RoomNotification(room: String, message: String) extends Notification
case class RoomMessageNotification(room: String, message: String, sender: String) extends Notification

case class JoinedRoomNotification(name: String) extends Notification