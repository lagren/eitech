package org.tenold.eitech

import akka.actor.Actor

import scala.collection.mutable

class UserRegistry extends Actor {

  val nicks: mutable.Set[String] = mutable.HashSet()

  def receive = {
    case Register(nickname) if nicks.contains(nickname) => sender() ! RegistrationFailed(nickname)
    case Register(nickname) =>
      nicks.add(nickname)
      sender() ! RegistrationSuccessful(nickname)
    case Unregister(nickname) =>
      nicks.remove(nickname)
    case msg => unhandled(msg)
  }

}

case class Register(nick: String)
case class Unregister(nick: String)

sealed trait RegistrationResult {
  def nick(): String
}

case class RegistrationSuccessful(nick: String) extends RegistrationResult
case class RegistrationFailed(nick: String) extends RegistrationResult