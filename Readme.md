# EiTeCh simple chat server

EiTeCh is a simple chat server written in Java and Scala. It uses telnet as a communication protocol.

## Usage instructions

Telnet to the server instance and follow instructions printed on the screen.
	
	telnet localhost 1234
	
Demo server

	telnet 34.250.39.239 1234
	
## Project structure

The project is using Maven as the build tool. All dependencies are available from public repositories. A default Maven 
installation should suffice.

## How to start the server

Using maven:

	mvn clean package exec:java
	
If you don't have Maven set up, [download](https://maven.apache.org/) and unpack. Make sure that the *mvn* binary is 
available on your PATH.

Java is also required. At least version 7.
	
## Architecture

The server uses [Akka](http://akka.io/) as framework to create an [actor model](https://en.wikipedia.org/wiki/Actor_model).

It is build up by the following main elements:

* A *client* is responsible to receive and print information to a network socket.
* An *user* that contains the user state and transport actions and notifications to and from a room.
* A *room* contains references to all participants in the room.
* An *action* is an upstream message from the client/user to the room.
* A *notification* is a downstream message from the room to the client/user.
* The *room dispatcher* is responsible for creating rooms and routing actions to the correct room.